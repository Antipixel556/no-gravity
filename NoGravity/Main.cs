using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace NoGravity
{
    public class Main : VTOLMOD
    {
        private bool _isDisabled, _onCoolDown;
        private float _coolDown = 2f;
        private float _currentTimer;
        private static Settings _setting;
        private static UnityAction<float> _amountChanged;
        private static float _gravityAmount = 0;

        public override void ModLoaded()
        {
            base.ModLoaded();
            _amountChanged += ChangedValue;

            _setting = new Settings(this);
            _setting.CreateCustomLabel("This will change the amount of gravity when turned on");
            _setting.CreateCustomLabel("Default = -9.3");
            _setting.CreateFloatSetting("Toggled Amount", _amountChanged, _gravityAmount);
            VTOLAPI.CreateSettingsMenu(_setting);
        }

        public void ChangedValue(float amount)
        {
            _gravityAmount = amount;
        }

        private void Update()
        {
            if (VRHandController.controllers.Count != 2)
                return;

            if (_onCoolDown)
            {
                _currentTimer += Time.deltaTime;
                if (_currentTimer >= _coolDown)
                {
                    _onCoolDown = false;
                    _currentTimer = 0;
                }
            }
            else if (VRHandController.controllers[0].thumbButtonPressed &&
                VRHandController.controllers[1].thumbButtonPressed)
            {
                Physics.gravity = new Vector3(0, _isDisabled ? _gravityAmount : -9.3f, 0);
                _isDisabled = !_isDisabled;
                Log("Set gravity to " + _isDisabled);
                _onCoolDown = true;
            }

        }
    }
}