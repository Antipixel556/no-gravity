# ![Website Preview Image](https://gitlab.com/vtolvr-mods/no-gravity/-/raw/master/web_preview.png)
# No Gravity
## This is the example mod, which adds the ability to toggle the gravity by pressing two buttons on you

This is the example mod, which adds the ability to toggle the gravity by pressing two buttons on your controllers at the same time.

You can download the mod from vtolvr-mods.com [here](https://vtolvr-mods.com/mod/jwu6447r/) to play with it in game, this is just the source code if you would like to read and understand how it works.

**Make sure to fill out the dependencies in `Dependencies/instructions.txt` to build the project**